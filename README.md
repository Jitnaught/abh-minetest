A Minetest mod that adds the Accelerated Back Hopping glitch from Half-Life 2.

It is not a perfect recreation, due to the differences between the HL2 and Minetest. To do the recreated glitch in Minetest, hold jump (space button) + left, right, and/or down (A/S/D buttons) (e.g. Space + S). You will gain speed on each jump.

Installation:
Copy abh folder to minetest-install-directory/mods/ in the official Windows releases and RUN-IN-PLACE versions on Linux, or to ~/.minetest/mods/ in globally installed Minetest versions. You will have to create these directories if they do not yet exist.
