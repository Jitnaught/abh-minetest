function player_moving_abh(player_controls, player_speed)
	return ((player_controls.down or player_controls.left or player_controls.right) and player_speed > 3.9 and not player_controls.up)
end

minetest.register_globalstep(function(dtime)
	local player_con, player_speed, node_pos, node

	for _,player in ipairs(minetest.get_connected_players()) do
		if player:get_attribute('was_on_ground') == nil then
			player:set_attribute('was_on_ground', "1")
			player:set_attribute('abh', "0")
		end

		local was_on_ground = (player:get_attribute('was_on_ground') == "1")
		local abh = (player:get_attribute('abh') == "1")

		player_con = player:get_player_control()
		player_speed = vector.length(player:get_player_velocity())

		node_pos = player:get_pos()
		node_pos.y = node_pos.y - 0.3

		node = minetest.get_node_or_nil(node_pos)

		if node == nil or node.name == "air" then
			if was_on_ground and abh and player_moving_abh(player_con, player_speed) then
				player:set_physics_override({ speed = player:get_physics_override().speed + 1.0 })	
			end

			was_on_ground = false
		else
			abh = player_con.jump and player_moving_abh(player_con, player_speed) 

			if not abh then
				player:set_physics_override({ speed = 1.0 })
			end

			was_on_ground = true
		end

		player:set_attribute('was_on_ground', was_on_ground and "1" or "0")
		player:set_attribute('abh', abh and "1" or "0")
	end
end)
